var http = require('http');

http.createServer( function(req,res){
  // normalize url by removing querystring, optional
  // trailing slash, and making it lowercase
  var path = req.url.replace(/\/?(?:\?.*)?$/, '').toLowerCase();
  switch(path){
    case '':
	   res.writeHead(200,{'Content-Type':'plain/text'})
	   res.end('Home Page\n');
    case '/about':
	   res.writeHead(200,{'Content-Type':'plain/text'})
	   res.end('About\n');
    default :
	   res.writeHead(404,{'Content-Type':'plain/text'})
	   res.end('Page Not Found\n');

  }
}).listen(3000)

console.log("Http Server started on 3000 port Press Ctrl-C to exit");
